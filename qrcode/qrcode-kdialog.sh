#!/bin/sh -eu

# SPDX-License-Identifier: Unlicense
# SPDX-FileCopyrightText: NONE
#
# This shell script requires to install kdialog,
# python3 and qrcode python module.
#

# First window
img_type=$(kdialog --title="QRCode generator" \
                   --geometry 310x100 \
                   --combobox "Format de l'image :" "PNG" "SVG" \
                   --default "PNG" \
           2> /dev/null)

# Save dialog box
if [ ! -z ${img_type} ]; then
    ext_img=$(echo ${img_type} | tr "[:upper:]" "[:lower:]")

    # https://bugs.kde.org/show_bug.cgi?id=467868
    if [ "${img_type}" = "PNG" ]; then
        mimetype="image/png"
    elif [ "${img_type}" = "SVG" ]; then
        mimetype="image/svg+xml"
    fi
    _args="--getsavefilename ${HOME} ${mimetype}"
    filename=$(kdialog ${_args} 2> /dev/null)

    if [ ! -z ${filename} ]; then
        #echo ${filename%.*}
        $(python3 tinyqrcode.py \
                  -o "${filename%.*}.${ext_img}" \
                  -t ${ext_img})
    else
        # Error window
        $(kdialog --error "Le nom de l'image n'a pas été renseigné.\nRelancez le programme" \
          2> /dev/null)
    fi
fi
