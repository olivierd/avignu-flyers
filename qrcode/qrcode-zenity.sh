#!/bin/sh -eu

# SPDX-License-Identifier: Unlicense
# SPDX-FileCopyrightText: NONE
#
# This shell script requires to install zenity (or zenity4),
# python3 and qrcode python module.
#

# Main window
forms=$(zenity --forms --title="QR Code generator" \
               --text="QR Code" \
               --add-combo="Format de l'image :" --combo-values="PNG|SVG" \
               --add-entry="Nom de l'image :")

# Analyze each fields
if [ ! -z ${forms} ]; then
    #
    ext_img=$(echo ${forms} | cut -d\| -f1 | tr "[:upper:]" "[:lower:]")

    # Name of future image
    name=$(echo ${forms} | cut -d\| -f2)
    if [ ! -z ${name} ]; then
        path_dir=$(zenity --file-selection \
                          --title="Sélectionnez un répertoire" --directory \
                          --filename=$HOME \
                   2> /dev/null)
        if [ ! -z ${path_dir} ]; then
            #echo "${path_dir}/${name%.*}.${ext_img}"
            $(python3 tinyqrcode.py \
                      -o "${path_dir}/${name%.*}.${ext_img}" \
                      -t ${ext_img})
        fi
    else
        # Error window
        $(zenity --error --title="Erreur" --ellipsize \
                 --text="Le nom de l'image n'a pas été renseigné.\nRelancez le programme" \
          2> /dev/null)
    fi
fi
