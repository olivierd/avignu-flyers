#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-License-Identifier: Unlicense
# SPDX-FileCopyrightText: NONE
#
# Small python script in order to create QR code image.
#

import argparse
import pathlib
import sys

try:
    import qrcode
    import qrcode.image.svg
except ImportError as err:
    print(err)
    sys.exit(-1)

class TinyQrCode(object):

    def __init__(self, output, image_type='png', content=None):
        if content is None:
            self.content = 'https://avignu.com/#info'
        else:
            self.content = content
        if image_type in ['png', 'svg']:
            self.img_type = image_type
        else:
            self.img_type = 'png'
        self.img_output = output

        # Control size of QR code (here 25x25 matrix)
        self.ver = 2
        self.error = qrcode.constants.ERROR_CORRECT_M

    def render(self):
        qr = qrcode.QRCode(error_correction=self.error,
                           version=self.ver)

        qr.add_data(self.content)

        if self.img_type == 'png':
            img = qr.make_image()
        elif self.img_type == 'svg':
            factory = qrcode.image.svg.SvgPathFillImage
            img = qr.make_image(image_factory=factory)

        img.save(self.img_output)

def normalize_mimetype(path_obj, ext):
    '''Check if it is the right extension, otherwise normalize
    path-like object.'''

    suffix = path_obj.suffix
    if suffix[1:] == ext:
        # Need to cast
        return '{0}'.format(path_obj)
    else:
        extension = '.{0}'.format(ext)
        return '{0}'.format(path_obj.with_suffix(extension))

def main(args):
    output = normalize_mimetype(pathlib.Path(args.file), args.type)

    qr_code = TinyQrCode(output, image_type=args.type, content=args.content)
    qr_code.render()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output',
                        dest='file', required=True,
                        help='QR Code image')
    parser.add_argument('-t', '--type', default='png',
                        help='type of image, PNG or SVG')
    parser.add_argument('-c', '--content',
                        help='data to insert')
    main(parser.parse_args())
